#!/usr/bin/env bash
set -e

/bin/cp /config/menus/local/* /config/menus/ &&
/usr/bin/node /srv/webapp/app.js &
/usr/sbin/dnsmasq --no-daemon --conf-file=/etc/dnsmasq.conf &
/usr/sbin/caddy run -environ -config /srv/Caddyfile &
wait -n