#!/usr/bin/env bash

set -x

export IMAGE=docker.io/library/alpine@sha256:234cb88d3020898631af0ccbbcca9a66ae7306ecd30c9720690858c1b007d2a0

ctr1=$(buildah from --arch "$ARCH" "$IMAGE")

# renovate: datasource=github-tags depName=netbootxyz/netboot.xyz
export NETBOOT_VERSION=2.0.40
echo $NETBOOT_VERSION > build/upstream-version
# renovate: datasource=github-tags depName=pftf/RPi4 versioning=loose
export PIPXE4_VERSION=v1.28
# renovate: datasource=github-tags depName=netbootxyz/webapp
export WEBAPP_VERSION=0.6.4
# renovate: datasource=repology depName=alpine_3_14/git versioning=loose
export GIT_VERSION=2.32.0-r0
# renovate: datasource=repology depName=alpine_3_14/caddy versioning=loose
export CADDY_VERSION=2.4.3-r0
# renovate: datasource=repology depName=alpine_3_14/coreutils versioning=loose
export CORE_UTILS_VERSION=8.32-r2
# renovate: datasource=repology depName=alpine_3_14/nodejs versioning=loose
export NODEJS_VERSION=14.17.1-r0
# renovate: datasource=repology depName=alpine_3_14/npm versioning=loose
export NPM_VERSION=7.17.0-r0 
# renovate: datasource=repology depName=alpine_3_14/catatonit versioning=loose
export CATATONIT_VERSION=0.1.5-r1 
# renovate: datasource=repology depName=alpine_3_14/bash versioning=loose
export BASH_VERSION=5.1.4-r0
# renovate: datasource=repology depName=alpine_3_14/dnsmasq versioning=loose
export DNSMASQ_VERSION=2.85-r2
# renovate: datasource=repology depName=alpine_3_14/libcap versioning=loose
export LIBCAP_VERSION=2.50-r0

buildah run "$ctr1" sh -c "
echo Install packages...\
&& apk add --no-cache \
    git=$GIT_VERSION \
    caddy=$CADDY_VERSION \
    coreutils=$CORE_UTILS_VERSION \
    nodejs=$NODEJS_VERSION \
    npm=$NPM_VERSION \
    catatonit=$CATATONIT_VERSION \
    bash=$BASH_VERSION \
    dnsmasq=$DNSMASQ_VERSION \
    libcap=$LIBCAP_VERSION \
&& setcap cap_net_bind_service=+ep /usr/sbin/dnsmasq \
&& echo grab netboot $NETBOOT_VERSION release artifacts... \
&& mkdir -p /config/menus/remote/ \
&& mkdir -p /config/menus/local/ \
&& mkdir -p /assets \
&& cd /config/menus/remote/ \
&& wget https://github.com/netbootxyz/netboot.xyz/releases/download/$NETBOOT_VERSION/menus.tar.gz \
&& wget https://github.com/netbootxyz/netboot.xyz/releases/download/$NETBOOT_VERSION/netboot.xyz-arm64.efi \
&& wget https://github.com/netbootxyz/netboot.xyz/releases/download/$NETBOOT_VERSION/netboot.xyz.kpxe \
&& wget https://github.com/netbootxyz/netboot.xyz/releases/download/$NETBOOT_VERSION/netboot.xyz-undionly.kpxe \
&& wget https://github.com/netbootxyz/netboot.xyz/releases/download/$NETBOOT_VERSION/netboot.xyz.efi \
&& wget https://github.com/netbootxyz/netboot.xyz/releases/download/$NETBOOT_VERSION/netboot.xyz-sha256-checksums.txt \
&& wget https://raw.githubusercontent.com/netbootxyz/netboot.xyz/$NETBOOT_VERSION/endpoints.yml \
&& sha256sum --check --ignore-missing netboot.xyz-sha256-checksums.txt \
&& mv endpoints.yml /config/endpoints.yml \
&& wget https://github.com/pftf/RPi4/releases/download/$PIPXE4_VERSION/RPi4_UEFI_Firmware_$PIPXE4_VERSION.zip \
&& unzip RPi4_UEFI_Firmware_$PIPXE4_VERSION.zip \
&& rm RPi4_UEFI_Firmware_$PIPXE4_VERSION.zip \
&& echo -n $NETBOOT_VERSION > /config/menuversion.txt \
&& tar xf menus.tar.gz -C /config/menus/remote \
&& rm -f menus.tar.gz \
&& cp -r /config/menus/remote/* /config/menus \
&& cd /srv/ \
&& git clone https://github.com/netbootxyz/webapp.git \
&& cd /srv/webapp \
&& git checkout 0.6.4 \
&& npm i npm \
&& npm install --prefix /srv/webapp \
&& echo setting permissions.. \
&& chown -R nobody:nobody /config \
&& chown -R nobody:nobody /assets \
&& chown -R nobody:nobody /srv/webapp \
&& echo Cleaning up...\
&& apk del --no-cache \
   git \
   npm \
   libcap \
&& rm /config/menus/RPI_EFI.fd
"
buildah copy --chown nobody:nobody "$ctr1" RPI_EFI.fd /config/menus
buildah copy --chown nobody:nobody "$ctr1" README.ipxe /config/menus/local/readme-kutara.ipxe
buildah copy "$ctr1" dnsmasq.conf /etc/dnsmasq.conf
buildah copy "$ctr1" Caddyfile /srv/Caddyfile
buildah copy "$ctr1" entrypoint.sh entrypoint.sh

buildah config \
--port 8080 \
--port 69/udp \
"$ctr1"

buildah config \
--user nobody \
"$ctr1"

buildah config \
--volume /config \
--volume /assets \
"$ctr1"

buildah config \
--env XDG_CONFIG_HOME=/tmp \
"$ctr1"

buildah config \
--label io.containers.autoupdate=image \
--label io.kutara.version.netboot=$NETBOOT_VERSION \
--label io.kutara.version.netboot-webapp=$WEBAPP_VERSION  \
--label io.kutara.version.pipxe4=$PIPXE4_VERSION \
"$ctr1"

buildah config --cmd '[ "catatonit", "--", "/entrypoint.sh"]' "$ctr1"