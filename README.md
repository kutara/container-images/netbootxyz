# netbootxyz

netboot.xyz container suited for arm64 and amd64

```
podman run -d \
  --name=netbootxyz \
  -p 69:69/udp \
  -p 8080:8080 `#WebGUI and /assets` \
  -v /path/to/assets:/assets `#optional` \
  --restart unless-stopped \
  quay.io/kutara/netbootxyz
```
