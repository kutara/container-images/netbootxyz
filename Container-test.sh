#!/usr/bin/env bash

set -x

echo running tests ensure to cleanup containers

bash -e ./Container.sh

CONTAINER=$(buildah containers --format "{{.ContainerName}}")
buildah commit "$CONTAINER" netbootxyz
buildah rm --all
